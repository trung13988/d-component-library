import React from 'react';

function Button(_a) {
    var content = _a.content;
    return React.createElement("button", { type: "button" }, content);
}

export { Button };
//# sourceMappingURL=index.es.js.map
